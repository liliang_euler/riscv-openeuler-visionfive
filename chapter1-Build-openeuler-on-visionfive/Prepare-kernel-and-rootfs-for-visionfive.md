# 操作手册：Prepare kernel and rootfs for visionfive

---

## 1 准备工作

在Ubuntu上安装工具链。

```bash
sudo apt install gcc-riscv64-linux-gnu
```

查看工具链，例如这里使用的gcc版本为9.4.0。

```bash
riscv64-linux-gnu-gcc -v
```

## 2 制作kernel image

### 2.1 kernel源码

linux5.4源码不支持visionfive，只能下载starfive的，这里下载的版本为5.17。

```bash
git clone https://github.com/starfive-tech/linux
```

### 2.2 编译

```bash
make CROSS_COMPILE=riscv64-linux-gnu- ARCH=riscv visionfive_defconfig
```

```bash
make CROSS_COMPILE=riscv64-linux-gnu- ARCH=riscv menuconfig
```

参考软件所提供的Qemu下的配置：

>CONFIG_NR_CPUS=32
>CONFIG_REISERFS_FS_XATTR=y
>CONFIG_REISERFS_FS_SECURITY=y
>CONFIG_EXT4_FS_SECURITY=y
>CONFIG_BRIDGE=y
>CONFIG_DRM_BOCHS=y
>CONFIG_INPUT_EVDEV=y

看起来相当于无需修改默认配置，但需确认没有勾选selinux，目前openeuler riscv对该项支持不够。

默认配置下，设置`CONFIG_BRIDGE=y`

配置文件参考：[.comfig](.config)

编译内核：

```bash
make CROSS_COMPILE=riscv64-linux-gnu- ARCH=riscv -j $(nproc)
```

得到`arch/riscv/boot/Image.gz`

```bash
make CROSS_COMPILE=riscv64-linux-gnu- ARCH=riscv dtbs
```

得到`arch/riscv/boot/dts/starfive/jh7100-starfive-visionfive-v1.dtb`

## 3 获取openeuler文件镜像

[openEuler-22.03.riscv64.qcow2](https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/development/2203/Image/openEuler-22.03.riscv64.qcow2)

## 4 安装kernel和openeuler文件系统到micro sd卡中

参考[《操作手册：Build openeuler on visionfive》](readme.md)，安装kernel。
