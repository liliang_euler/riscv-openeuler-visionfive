# 操作手册：Prepare micro sd card for visionfive

---

## 1 目标

使用fdisk将micro sd卡分为两个分区：第一分区格式为 FAT32，大小 500M，用来安装Linux内核，为启动分区。第二个分区为 ext4，占剩下的所有空间，用来安装 openeuler的文件系统，为文件系统分区。

## 2 操作过程

### 2.1 使用fdisk分区

查看SD卡磁盘情况，找出micro sd卡的设备名。这里的系统上micro sd卡的设备名是：/dev/sdb，包含1个分区，分区名是sdb1。使用命令查看系统上的存储设备。

```bash
lsblk
```

使用fdisk分区

```bash
sudo fdisk /dev/sdb
```

可输入m查看分区指令，输入d删除已有分区，1表示只有这第1个分区，即sdb1。

```bash
分区1已删除
```

输入 n 新建一个分区，输入 p 选择使用主分区，输入 1 表示这是第一个分区，按回车来使用默认的开始扇区，输入 +500M 来分配分区大小，创建类型为Linux的分区。

```bash
创建了1个新分区1，类型为“Linux”，大小为500MB。
```

输入 n 再新建一个分区，输入 p 选择使用主分区，输入 2 表示这是第二个分区，回车两次使用默认大小，创建类型为Linux的分区。

```bash
创建了1个新分区2，类型为“Linux”，大小为59GB。
```

### 2.2 设置启动标志和分区类型

输入命令a，输入分区号1，设置1号分区为启动分区，输入命令t，输入分区号1以及Hex代码c，将1号分区的类型更改为FAT32。

```bash
已将分区“Linux”的类型更改为“W95 FAT32（LBA）”。
```

输入 p 查看分区情况，显示/dev/sdb1为启动分区，类型为FAT32；/dev/sdb2类型为Linux。

输入 w 写入分区信息，提示reboot后生效。

格式化各个分区：

```bash
sudo mkfs -V -t vfat /dev/sdb1
```

```bash
sudo mkfs.ext4 -L rootfs /dev/sdb2
```

## 3 查看分区状态

查看详细分区情况：

```bash
sudo fdisk -l
```

    设备        启动    大小     类型
    /dev/sdb1   *      500MB    W95 FAT32（LBA）
    /dev/sdb2          59GB     Linux

查看设备情况：

```bash
df -h
```

    文件系统    容量     挂载点
    /dev/sdb1   500M    /media/riscv/715D-A517
    /dev/sdb2   58G     /media/riscv/rootfs

至此，两个分区就建立好了，其中sdb1用于放置kernel image，sdb2用于放置文件系统。

## 4 安装kernel和openeuler文件系统到micro sd卡中

参考[《操作手册：Build openeuler on visionfive》](readme.md)，安装kernel和openeuler文件系统。
