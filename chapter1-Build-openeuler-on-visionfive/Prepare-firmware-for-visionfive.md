# 操作手册：Prepare firmware for visionfive

---

## 1 准备工作

在Ubuntu上安装工具链。

```bash
sudo apt install gcc-riscv64-linux-gnu
```

查看工具链，例如这里使用的gcc版本为9.4.0。

```bash
riscv64-linux-gnu-gcc -v
```

## 2 编译bootloader

### 2.1 bootloader源码

下载bootloader源码

```bash
git clone https://github.com/starfive-tech/JH7100_secondBoot.git
```

### 2.2 编译

```bash
cd JH7100_secondBoot/build
make
```

得到`bootloader-JH7100-220211.bin.out`

## 3 编译ddrinit

### 3.1 ddrinit源码

```bash
git clone https://github.com/starfive-tech/JH7100_ddrinit.git
```

### 3.2 编译

```bash
cd JH7100_ddrinit/build
make
```

得到`ddrinit-2133-220211.bin.out`

## 4 编译u-boot

### 4.1 u-boot源码

下载u-boot源码。

```bash
git clone https://github.com/starfive-tech/u-boot
```

```bash
cd u-boot
git checkout -b JH7100_upstream origin/JH7100_upstream
git pull
```

确认配置为从micro sd启动。

`u-boot/configs/ starfive_jh7100_visionfive_smode_defconfig:35:CONFIG_PREBOOT
="run mmcbootenv"`

修改代码（本文提供已完成修改的代码）

[u-boot/include/configs/starfive-jh7100.h](starfive-jh7100.h)

### 4.2 编译

```bash
make starfive_jh7100_visionfive_smode_defconfig ARCH=riscv CROSS_COMPILE=riscv64-linux-gnu-
```

```bash
make u-boot.bin u-boot.dtb ARCH=riscv CROSS_COMPILE=riscv64-linux-gnu-
```

`u-boot.dtb`和`u-boot.bin`将被用于编译OpenSBI。

## 5 编译OpenSBI

### 5.1 OpenSBI源码

下载OpenSBI源码。

```bash
git clone https://github.com/riscv/opensbi.git
```

### 5.2 编译

```bash
make ARCH=riscv CROSS_COMPILE=riscv64-linux-gnu- PLATFORM=generic FW_PAYLOAD_PATH=/home/riscv/riscv64-linux/u-boot/u-boot.bin FW_FDT_PATH=/home/riscv/riscv64-linux/u-boot/u-boot.dtb
```

进入fw_payload.bin所在目录，查看fw_payload.bin

```bash
cd opensbi/build/platform/generic/firmware
```

事先在工作目录中新建payload目录，将这里的fw_payload.bin拷贝进去。

```bash
cp fw_payload.bin /home/riscv/riscv64-linux/payload/
```

进入payload目录，编辑批处理程序`fsz.sh`

```bash
#!/bin/bash

function handle_file {
 inFile=$1
 echo inFile: $inFile
 outFile=$inFile.out

 inSize=`stat -c "%s" $inFile`
 inSize32HexBe=`printf "%08x\n" $inSize`
 inSize32HexLe=${inSize32HexBe:6:2}${inSize32HexBe:4:2}${inSize32HexBe:2:2}${inSize32HexBe:0:2}
 echo "inSize: $inSize (0x$inSize32HexBe, LE:0x$inSize32HexLe)"

 echo $inSize32HexLe | xxd -r -ps > $outFile
 cat $inFile >> $outFile
 echo outFile: $outFile

 outSize=`stat -c "%s" $outFile`
 outSize32HexBe=`printf "%08x\n" $outSize`
 echo "outSize: $outSize (0x$outSize32HexBe)"
}

if [ "$1" = "" -o "$1" = "--help" ]; then
 echo "Add file size(32bits, Little Endian) before the content."
 echo "Usage: ./fsz.sh <file>"
 exit 1
fi

handle_file "$@"
```

给执行权限

```bash
chmod 777 fsz.sh
```

将fw_payload.bin转换为fw_payload.bin.out

```bash
./fsz.sh fw_payload.bin fw_payload.bin.out
```

得到`fw_payload.bin.out`

## 4 安装Firmware到板载Flash中

参考[《操作手册：Build openeuler on visionfive》](readme.md)，安装bootloader ddrinit u-boot。
