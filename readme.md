# 《Play with OpenEuler on VisionFive》

---

## 1 课程介绍

- 课程名称：Play with OpenEuler on VisionFive

- 适用范围：计算机专业学员、OpenEuler RISCV SDK软件工程师

- 培训目标：使学员在VisionFive硬件平台上具备熟练部署RISC-V openEuler软件的能力。

## 2 课程内容

- 第一章 OpenEuler RISCV的安装
  - [操作手册：Build openeuler on visionfive](chapter1-Build-openeuler-on-visionfive/readme.md)
  - [操作手册：Prepare firmware for visionfive](chapter1-Build-openeuler-on-visionfive/Prepare-firmware-for-visionfive.md)
  - [操作手册：Prepare kernel and rootfs for visionfive](chapter1-Build-openeuler-on-visionfive/Prepare-kernel-and-rootfs-for-visionfive.md)
  - [操作手册：Prepare micro sd card for visionfive](chapter1-Build-openeuler-on-visionfive/Prepare-micro-sd-card-for-visionfive.md)

- 第二章 OpenEuler RISCV GUI APP的验证
  - 操作手册：Verify OpenEuler GUI APP on VisionFive

- 第三章 OpenEuler RISCV HPC APP的测试

- 第四章 OpenEuler RISCV 的优化
